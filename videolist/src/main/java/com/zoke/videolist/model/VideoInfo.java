package com.zoke.videolist.model;

import android.graphics.Bitmap;

/**
 * @author 大熊
 */
public class VideoInfo {
    public int imageId;
    public int bucketId;
    public String bucketName;
    public String path;
    public long dateTaken;
    public String resolution;
    public String size;
    public String displayname;
    public String duration;
    public Bitmap curThumb;

    public VideoInfo(int imageId, int bucketId, String bucketName,
                     String path, long dateTaken, String resolution, String size,
                     String displayname, String duration, Bitmap curThumb) {
        this.imageId = imageId;
        this.bucketId = bucketId;
        this.bucketName = bucketName;
        this.path = path;
        this.dateTaken = dateTaken;
        this.resolution = resolution;
        this.size = size;
        this.displayname = displayname;
        this.duration = duration;
        this.curThumb = curThumb;
    }

    @Override
    public String toString() {
        return "VideoInfo{" +
                "imageId=" + imageId +
                ", bucketId=" + bucketId +
                ", bucketName='" + bucketName + '\'' +
                ", path='" + path + '\'' +
                ", dateTaken=" + dateTaken +
                ", resolution='" + resolution + '\'' +
                ", size='" + size + '\'' +
                ", displayname='" + displayname + '\'' +
                ", duration='" + duration + '\'' +
                ", curThumb=" + curThumb +
                '}';
    }
}
