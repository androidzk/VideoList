package com.zoke.videolist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * @author 大熊
 */
public class BaseFragment extends Fragment {
    private LayoutInflater inflater;
    private View contentView;
    private ViewGroup container;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        onCreateView(savedInstanceState);
        if (contentView == null)
            return super.onCreateView(inflater, container, savedInstanceState);
        return contentView;
    }

    /**
     * 子类实现该方法 简化fragment添加View的方法 等同于fragment的onCreateView
     **/
    protected void onCreateView(Bundle savedInstanceState) {
    }

    /**
     * 模仿Activity查找控件
     **/
    public <T extends View> T findViewById(int id) {
        return (T) contentView.findViewById(id);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        contentView = null;
        container = null;
        inflater = null;

    }


    /**
     * 仿照Acitivity添加布局的方式
     **/
    public void setContentView(int layoutResID) {
        setContentView(inflater.inflate(layoutResID, container, false));
    }

    public void setContentView(View view) {
        contentView = view;
    }

    public View getContentView() {
        return contentView;
    }


}
