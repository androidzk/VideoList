package com.zoke.videolist.util;

import android.util.SparseArray;
import android.view.View;

/**
 * @author 大熊
 * @version 1.0
 * @fileName ViewHolderUtils.java
 * @description ViewHolder简化工具类
 * @email 651319154@qq.com
 */
public class ViewHolder {
    private ViewHolder() {
    }

    /**
     * ViewHolder简化方法 如:TextView mName =
     * (TextView)View.get(convertView,R.id.text); 直接这样用就可以了
     * <p>
     * convertView
     *
     * @param id 控件的id
     * @return 返回控件
     */
    public static <T extends View> T get(View convertView, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            convertView.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null) {
            childView = convertView.findViewById(id);
            viewHolder.put(id, childView);
        }
        return (T) childView;
    }
}
