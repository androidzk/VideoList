package com.zoke.videolist.util;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.zoke.videolist.listener.LoadVideoListener;
import com.zoke.videolist.model.VideoInfo;

import java.util.ArrayList;

/**
 * @author 大熊
 */
public class PhoneMediaVideoHelper {
    private PhoneMediaVideoHelper() {
        videoInfos = new ArrayList<>();
    }

    private static class PhoneMediaVideoLoader {
        private static final PhoneMediaVideoHelper INSTANCE = new PhoneMediaVideoHelper();
    }

    public static PhoneMediaVideoHelper getInstance() {
        return PhoneMediaVideoLoader.INSTANCE;
    }

    private ArrayList<VideoInfo> videoInfos;//

    private LoadVideoListener mListener;

    public void setLoadVideoListener(LoadVideoListener listener) {
        this.mListener = listener;
    }

    private static final String[] projectionPhotos = {
            MediaStore.Video.Media._ID, MediaStore.Video.Media.BUCKET_ID,
            MediaStore.Video.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Video.Media.DATA, MediaStore.Video.Media.DATE_TAKEN,
            MediaStore.Video.Media.RESOLUTION, MediaStore.Video.Media.SIZE,
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.DURATION,};

    public void loadAllVideoMedia(final Context context) {
        videoInfos.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor cursor = null;
                try {
                    cursor = MediaStore.Video.query(context.getContentResolver(), MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projectionPhotos);
                    if (cursor != null) {
                        int videoIdColumn = cursor.getColumnIndex(MediaStore.Video.Media._ID);
                        int bucketIdColumn = cursor.getColumnIndex(MediaStore.Video.Media.BUCKET_ID);
                        int bucketNameColumn = cursor.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
                        int dataColumn = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
                        int dateColumn = cursor.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN);
                        int resolutionColumn = cursor.getColumnIndex(MediaStore.Video.Media.RESOLUTION);
                        int sizeColumn = cursor.getColumnIndex(MediaStore.Video.Media.SIZE);
                        int displaynameColumn = cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME);
                        int durationColumn = cursor.getColumnIndex(MediaStore.Video.Media.DURATION);
                        if (cursor.getCount() >= 1) {
                            while (cursor.moveToNext()) {
                                int imageId = cursor.getInt(videoIdColumn);
                                int bucketId = cursor.getInt(bucketIdColumn);
                                String bucketName = cursor.getString(bucketNameColumn);
                                String path = cursor.getString(dataColumn);
                                long dateTaken = cursor.getLong(dateColumn);
                                String resolution = cursor.getString(resolutionColumn);
                                String size = cursor.getString(sizeColumn);
                                String displayname = cursor.getString(displaynameColumn);
                                String duration = cursor.getString(durationColumn);
                                VideoInfo mVideoDetails = new VideoInfo(imageId, bucketId, bucketName, path, dateTaken, resolution, size, displayname, duration, null);
                                videoInfos.add(mVideoDetails);
                            }
                        }
                        if (mListener != null)
                            mListener.onSuccess(videoInfos);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (mListener != null)
                        mListener.onError(e);
                } finally {
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e) {
                            if (mListener != null)
                                mListener.onError(e);
                        }
                    }
                }
            }
        }).start();
    }


}
