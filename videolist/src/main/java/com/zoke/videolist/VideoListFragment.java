package com.zoke.videolist;


import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.zoke.videolist.adapter.VideoAdapter;
import com.zoke.videolist.listener.LoadVideoListener;
import com.zoke.videolist.listener.OnVideoItemClickListener;
import com.zoke.videolist.model.VideoInfo;
import com.zoke.videolist.util.PhoneMediaVideoHelper;

import java.util.ArrayList;

/**
 * @author 大熊
 */
public class VideoListFragment extends BaseFragment implements LoadVideoListener, AdapterView.OnItemClickListener {
    private GridView mGridView;
    private TextView searchEmptyView;
    private VideoAdapter mAdapter;
    private ArrayList<VideoInfo> mList = new ArrayList<>();
    private OnVideoItemClickListener mVideoItemClick;
    private LoadVideoListener mLoadVideoListener;

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        super.onCreateView(savedInstanceState);
        setContentView(R.layout.fragment_videolist);
        mGridView = findViewById(R.id.grid_view);
        searchEmptyView = findViewById(R.id.searchEmptyView);
        mAdapter = new VideoAdapter(getActivity(), mList);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        PhoneMediaVideoHelper.getInstance().setLoadVideoListener(this);
        PhoneMediaVideoHelper.getInstance().loadAllVideoMedia(getActivity());
    }


    /**
     * 获取GridView
     *
     * @return
     */
    public GridView getGridView() {
        return mGridView;
    }

    public void setOnVideoItemClickListener(OnVideoItemClickListener listener) {
        this.mVideoItemClick = listener;
    }

    /**
     * onSuccess onError
     *
     * @param listener
     */
    public void setLoadVideoListener(LoadVideoListener listener) {
        this.mLoadVideoListener = listener;
    }

    @Override
    public void onSuccess(final ArrayList<VideoInfo> arrVideoDetails) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mLoadVideoListener != null)
                    mLoadVideoListener.onSuccess(arrVideoDetails);
                if (arrVideoDetails != null && arrVideoDetails.size() != 0) {
                    mList.clear();
                    for (VideoInfo info : arrVideoDetails) {
                        mList.add(info);
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    mGridView.setVisibility(View.GONE);
                    searchEmptyView.setText("没有视频哦");
                    searchEmptyView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onError(final Exception e) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mLoadVideoListener != null)
                    mLoadVideoListener.onError(e);
                mGridView.setVisibility(View.GONE);
                searchEmptyView.setText("加载出错");
                searchEmptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mVideoItemClick != null) {
            VideoInfo info = mList.get(position);
            mVideoItemClick.onVideoItemClick(parent, view, position, info);
        }
    }
}
