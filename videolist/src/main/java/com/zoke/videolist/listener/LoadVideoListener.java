package com.zoke.videolist.listener;

import com.zoke.videolist.model.VideoInfo;

import java.util.ArrayList;

/**
 * @author 大熊
 */
public interface LoadVideoListener {
    void onSuccess(ArrayList<VideoInfo> arrVideoDetails);//成功

    void onError(Exception e);//抛异常
}
