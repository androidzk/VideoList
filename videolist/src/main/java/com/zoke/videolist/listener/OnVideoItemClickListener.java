package com.zoke.videolist.listener;

import android.view.View;
import android.widget.AdapterView;

import com.zoke.videolist.model.VideoInfo;

/**
 * Created by bear on 16/5/4.
 */
public interface OnVideoItemClickListener {
    void onVideoItemClick(AdapterView<?> parent, View view, int position, VideoInfo videoInfo);
}
