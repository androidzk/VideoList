package com.zoke.videolist.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * 适配器基类
 *
 * @author JackWu
 */
public abstract class BaseObjectListAdapter extends BaseAdapter {

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected List<? extends Object> mDatas;// 数据集合

    public BaseObjectListAdapter(Context context, List<? extends Object> datas) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        if (datas != null) {
            mDatas = datas;
        }
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getItemView(position, convertView, parent);
    }

    /**
     * 子类必须实现的处理getView的方法
     **/
    public abstract View getItemView(int position, View convertView,
                                     ViewGroup parent);

    public List<? extends Object> getDatas() {
        return mDatas;
    }

    /**
     * 获取convertView
     **/
    protected View getConvertView(View convertView, int resid) {
        if (null == convertView) {
            convertView = mInflater.inflate(resid, null);
        }
        return convertView;
    }
}
