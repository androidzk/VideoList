package com.zoke.videolist.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoke.videolist.R;
import com.zoke.videolist.model.VideoInfo;
import com.zoke.videolist.util.VideoThumbLoader;
import com.zoke.videolist.util.ViewHolder;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author 大熊
 */
public class VideoAdapter extends BaseObjectListAdapter {
    private VideoThumbLoader thumbLoader;

    public VideoAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
        this.thumbLoader = new VideoThumbLoader(mContext);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.gridview_item_video);
        ImageView media_photo_image = ViewHolder.get(convertView, R.id.media_photo_image);
        TextView album_name = ViewHolder.get(convertView, R.id.album_name);
        TextView album_count = ViewHolder.get(convertView, R.id.album_count);
        VideoInfo info = (VideoInfo) mDatas.get(position);
        album_name.setText(info.displayname);
        try {
            album_count.setVisibility(View.VISIBLE);
            album_count.setText(formatFileSize(Long.parseLong(info.size)));
        } catch (Exception e) {
            album_count.setVisibility(View.GONE);
        }
        thumbLoader.DisplayImage("" + info.imageId, media_photo_image, null);
        return convertView;
    }

    /**
     * 换算文件大小
     *
     * @param size
     * @return
     */
    public String formatFileSize(long size) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (size < 1024) {
            fileSizeString = df.format((double) size) + "B";
        } else if (size < 1048576) {
            fileSizeString = df.format((double) size / 1024) + "K";
        } else if (size < 1073741824) {
            fileSizeString = df.format((double) size / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) size / 1073741824) + "G";
        }
        return fileSizeString;
    }
}
