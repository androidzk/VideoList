package com.zoke.videolist.demo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

import com.zoke.videolist.VideoListFragment;
import com.zoke.videolist.listener.OnVideoItemClickListener;
import com.zoke.videolist.model.VideoInfo;

/**
 * 查找视频列表库，使用范例 以Fragment的形式提供界面 TODO:待做多选形式，待完善
 */
public class MainActivity extends AppCompatActivity implements OnVideoItemClickListener {
    private VideoListFragment videoListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoListFragment = new VideoListFragment();
        videoListFragment.setOnVideoItemClickListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, videoListFragment).commit();
    }

    @Override
    public void onVideoItemClick(AdapterView<?> parent, View view, int position, VideoInfo videoInfo) {
        //TODO:这里处理选择视频的操作 留给开发者 --示例调用系统方式打开视频
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(videoInfo.path), "video/*");
        startActivity(intent);
    }
}
