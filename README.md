#效果图
![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/105354_7c59e7a1_312885.png "在这里输入图片标题")

#使用范例
```
VideoListFragment videoListFragment = new VideoListFragment();
videoListFragment.setOnVideoItemClickListener(this);
getSupportFragmentManager().beginTransaction().replace(R.id.container, videoListFragment).commit();
```
```
@Override
    public void onVideoItemClick(AdapterView<?> parent, View view, int position, VideoInfo videoInfo) {
        //TODO:这里处理选择视频的操作 留给开发者 --示例调用系统方式打开视频
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(videoInfo.path), "video/*");
        startActivity(intent);
    }
```
#待完善功能，多选视频功能，现在只能一次选择一个哦。
